# vmapprentis-deploy

## What does it do ?

vmapprentis-deploy is a project that aims to deploy prometheus and grafana containers on the apprentice virtual machine.

Of course it is possible in the future to contribute to this project in order to deploy more.

## Where can I find/add secrets related to this project ?

On **keybase** !

If you are of our team of course, path to secrets file is `/keybase/team/epfl_dojo.apprentis/vm-apprentis.`

Every secrets will be in it.

_As of now_ there's only the login information for the currently deployed **grafana container**. When accessing grafana, a prompt will ask
you for a user and a password. (prometheus doesn't seem to need login info by default).

So that's where you'll find that login !

## How can I use this project ?

Go into the `ansible/` folder.

You'll find a `.sh` file in it called `apprsible`.

### Basic launch

Here are the two steps necessary to launch this project when you're in the right directory.

1. Make apprsible executable

```
chmod +x apprisble
```

2. Launch it

```
./apprisble
```

### Force reload containers

If you have made any changes to the deployment of already deployed containers in this project, you'll probably need to do a reload in order for changes to take place.

For that I added a simple argument to facilitate it.

You can do a :

```
./apprsible -r
```

**OR**

```
./apprsible --reload
```

It'll make every containers reload.

Of course if you want that argument to work on your new containers deployments you'll need to set something up.

Here's what :

1. Add a `recreate:` config as it follows

```yml
recreate: >-
  {{ "force-reload-all" in ansible_run_tags }}
```

2. Then at the end add this tag, like this

```yml
tags:
  - force-reload-all
```

### More specific commands

Of course, `apprsible` is just a tool to facilitate your usage of ansible for this specific project. So it does some part of the
`ansible-playbook` command for you.

**It is still possible** to add normal `ansible-playbook` arguments after the command. It's just as simple as using the basic command.

So if you want to know how to make your command more specific with more options, you can look into ansible-playbook's `man` !

#### Example

```
./apprsible -t prometheus
```

_Only launches tasks that have the `prometheus` tag_

---

Here you go, have fun !
